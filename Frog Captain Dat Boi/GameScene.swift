//
//  GameScene.swift
//  Frog Captain Dat Boi
//
//  Created by Geddy on 14/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import SpriteKit
import GameplayKit

var player = SKSpriteNode()
var projectile = SKSpriteNode()
var enemy = SKSpriteNode()
var stars = SKSpriteNode()
var scoreLabel = SKLabelNode()
var mainLabel = SKLabelNode()

var playerSize = CGSize(width: 70, height: 70)
var projectileSize = CGSize(width: 10, height: 10)
var enemySize = CGSize(width: 40, height: 40)
var starSize = CGSize()
var offBlackColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1.0)
var offWhiteColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
var orangeCustomColor = UIColor.orange

var fireProjectileRate = 0.2
var projectileSpeed = 0.9

var enemySpeed = 2.1
var enemySpawnRate = 0.6

var isAlive = true
var score = 0
var touchLocation = CGPoint()

struct physicsCategory {
    static let player: UInt32 = 0
    static let projectile: UInt32 = 1
    static let enemy: UInt32 = 2
}


class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        self.backgroundColor = offBlackColor
        resetGameVariables()
        spawnPlayer()
        spawnMainLabel()
        spawnScoreLabel()
        fireProjectile()
        timerSpawnEnemies()
        timerStarSpawn()
        // Get label node from scene and store it for use later
        
        // Create shape node to use during mouse interaction
    }
    
    func resetGameVariables() {
        isAlive = true
        score = 03    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches {
            touchLocation = t.location(in: self)
    }
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            touchLocation = touch.location(in: self)
            if isAlive == true {
                movePlayerOnTouch()
            }
        }
    }
    
    func movePlayerOnTouch() {
        player.position.x = touchLocation.x
    }
    func spawnPlayer() {
        player = SKSpriteNode(imageNamed: "shipPlayer")
        player.position = CGPoint(x: self.frame.midX, y: self.frame.midY - 130)
        player.physicsBody = SKPhysicsBody(texture: player.texture!, size: player.size)
        player.physicsBody?.affectedByGravity = false
        player.physicsBody?.categoryBitMask = physicsCategory.player
        player.physicsBody?.contactTestBitMask = physicsCategory.enemy
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.isDynamic = true
        
        player.name = "playerName"
        
        self.addChild(player)
        
        
    }
    
    func spawnProjectile() {
        projectile = SKSpriteNode(imageNamed: "projectile")
        projectile.position = CGPoint(x: player.position.x, y: player.position.y)
        projectile.physicsBody = SKPhysicsBody(rectangleOf: projectile.size)
        projectile.physicsBody?.affectedByGravity = false
        projectile.physicsBody?.categoryBitMask = physicsCategory.projectile
        projectile.physicsBody?.contactTestBitMask = physicsCategory.enemy
        projectile.physicsBody?.isDynamic = true
        
        projectile.name = "projectileName"
        projectile.zPosition = -1
        moveProjectileToTop()
        self.addChild(projectile)
    }
    func moveProjectileToTop() {
        let moveForward = SKAction.moveTo(y: 1000, duration: projectileSpeed)
        let destroy = SKAction.removeFromParent()
        
        projectile.run(SKAction.sequence([moveForward, destroy]))
    }
    
    func spawnEnemy() {
        
        let randomX = Int(arc4random_uniform(500) + 300)
        
        enemy = SKSpriteNode(imageNamed: "enemy")
        enemy.position = CGPoint(x: randomX - 300, y: 1000)
        enemy.physicsBody = SKPhysicsBody(rectangleOf: enemy.size)
        enemy.physicsBody?.affectedByGravity = false
        enemy.physicsBody?.isDynamic = true
        enemy.physicsBody?.allowsRotation = false
        enemy.physicsBody?.categoryBitMask = physicsCategory.enemy
        enemy.physicsBody?.collisionBitMask = physicsCategory.projectile | physicsCategory.player
        
        enemy.name = "enemyName"
        enemy.zPosition = -1
        moveEnemyToFloor()
        
        self.addChild(enemy)
    }
    
    func moveEnemyToFloor() {
        let moveTo = SKAction.moveTo(y: -300, duration: enemySpeed)
        let destroy = SKAction.removeFromParent()
        
        enemy.run(SKAction.sequence([moveTo, destroy]))
    }
    
    func spawnStar() {
        let randomSize = Int(arc4random_uniform(4) + 15)
        let randomX = Int(arc4random_uniform(1000))
        starSize = CGSize(width: randomSize, height: randomSize)
        stars = SKSpriteNode(color: offWhiteColor, size: starSize)
        stars.size = starSize
        stars.position = CGPoint(x: -randomX, y: 1000)
        stars.zPosition = -2
        
        starsMove()
        self.addChild(stars)
    }
    
    func starsMove() {
        let randomTime = Int(arc4random_uniform(10))
        let doubleRandomTime: Double = Double(randomTime)
        
        let moveTo = SKAction.moveTo(y: -400, duration: doubleRandomTime)
        let destroy = SKAction.removeFromParent()
        
        stars.run(SKAction.sequence([moveTo, destroy]))
    }
    
    func spawnMainLabel() {
        mainLabel = SKLabelNode(fontNamed: "Futura")
        mainLabel.fontSize = 100
        mainLabel.fontColor = offWhiteColor
        mainLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY + 200)
        mainLabel.text = "Start"
        self.addChild(mainLabel)
        
    }
    
    func spawnScoreLabel() {
        scoreLabel = SKLabelNode(fontNamed: "Futura")
        scoreLabel.fontSize = 40
        scoreLabel.fontColor = offWhiteColor
        scoreLabel.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 30)
        scoreLabel.text = "Score: \(score)"
        self.addChild(scoreLabel)
    }
    
    func fireProjectile() {
        let timer = SKAction.wait(forDuration: fireProjectileRate)
        let spawn = SKAction.run {
            if isAlive == true {
                self.spawnProjectile()
            }
            
        }
        
        let sequence = SKAction.sequence([timer, spawn])
        self.run(SKAction.repeatForever(sequence))
    }
    
    
    func timerSpawnEnemies() {
        let wait = SKAction.wait(forDuration: enemySpawnRate)
        let spawn = SKAction.run {
            
            if isAlive == true {
                self.spawnEnemy()
            }
        }
        
        let sequence = SKAction.sequence([wait, spawn])
        self.run(SKAction.repeatForever(sequence))
        
    }
    
    func timerStarSpawn() {
        let wait = SKAction.wait(forDuration: 0.2)
        let spawn = SKAction.run {
            if isAlive {
                self.spawnStar()
                self.spawnStar()
                self.spawnStar()
                self.spawnStar()
            }
        }
        
        let sequence = SKAction.sequence([wait, spawn])
        self.run(SKAction.repeatForever(sequence))
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        print("collided with object")
        if ((firstBody.categoryBitMask == physicsCategory.enemy) && (secondBody.categoryBitMask == physicsCategory.projectile) || (firstBody.categoryBitMask == physicsCategory.projectile) && (secondBody.categoryBitMask == physicsCategory.enemy)) {
            spawnExplosion(enemyTemp: firstBody.node as! SKSpriteNode)
            
            enemyProjectileCollision(contactA: firstBody.node as! SKSpriteNode, contactB: secondBody.node as! SKSpriteNode)
            
        }
        
        if ((firstBody.categoryBitMask == physicsCategory.enemy) && (secondBody.categoryBitMask == physicsCategory.player) || (firstBody.categoryBitMask == physicsCategory.player) && (secondBody.categoryBitMask == physicsCategory.enemy)) {
            playerEnemyCollision(contactA: firstBody.node as! SKSpriteNode, contactB: secondBody.node as! SKSpriteNode)
            print("died")
        }
        
        
    }
    
    
    
    func enemyProjectileCollision(contactA: SKSpriteNode, contactB: SKSpriteNode) {
        if contactA.name == "enemyName" && contactB.name == "projectileName" {
            score += 1
            print("Collided")
            
            let fadeOut = SKAction.fadeOut(withDuration: 0.1)
            let destroy = SKAction.removeFromParent()
            
            contactA.run(SKAction.sequence([fadeOut, destroy]))
            
            contactA.removeFromParent()
            updateScore()
            
            
            
        } else if contactB.name == "enemyName" && contactA.name == "projectileName" {
            score += 1
            let fadeOut = SKAction.fadeOut(withDuration: 0.1)
            let destroy = SKAction.removeFromParent()
            
            contactB.run(SKAction.sequence([fadeOut, destroy]))
            
            contactA.removeFromParent()
            print("Collided")
            updateScore()
            
        }
    }
    
    func playerEnemyCollision(contactA: SKSpriteNode, contactB: SKSpriteNode) {
        if contactA.name == "enemyName" && contactB.name == "playerName" {
            isAlive = false
            gameOverLogic()
            print(isAlive)
        } else if contactB.name == "enemyName" && contactA.name == "playerName" {
            isAlive = false
            gameOverLogic()
            print(isAlive)
        }
    }
    func spawnExplosion(enemyTemp: SKSpriteNode) {
        let explosionEmmiterPath = Bundle.main.path(forResource: "particleSpark", ofType: "sks")
        let explosion = NSKeyedUnarchiver.unarchiveObject(withFile: explosionEmmiterPath!) as! SKEmitterNode
        
        explosion.position = enemyTemp.position
        
        explosion.zPosition = 1
        explosion.targetNode = self
        self.addChild(explosion)
        
        
        
        let wait = SKAction.wait(forDuration: 0.5)
        let removeExplosion = SKAction.run {
            explosion.removeFromParent()
        }
        
        let sequence = SKAction.sequence([wait, removeExplosion])
        self.run(sequence)
    }
    
    
    func gameOverLogic() {
        //TODO set up game over
        mainLabel.fontSize = 55
        mainLabel.text = "Unicycle Spaceship Failed!"
        resetGame()
    }
    
    func resetGame() {
        let wait = SKAction.wait(forDuration: 1.0)
        let theTitleScene = TitleScene(fileNamed: "TitleScene")
        theTitleScene?.scaleMode = .aspectFill
        
        let changeScene = SKAction.run {
            self.scene?.view?.presentScene(theTitleScene!, transition: .doorway(withDuration: 0.4))
            
        }
        
        let sequence = SKAction.sequence([wait, changeScene])
        self.run(SKAction.repeat(sequence, count: 1))
    }
    
    func updateScore() {
        scoreLabel.text = "Score: \(score)"
    }
    
    func movePlayerOffScreen() {
        player.position.x = self.frame.minX - 300
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if isAlive == false {
            movePlayerOffScreen()
        }
    }
}
