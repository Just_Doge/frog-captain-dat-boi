//
//  TitleScene.swift
//  Frog Captain Dat Boi
//
//  Created by Geddy on 15/4/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import Foundation
import SpriteKit

var btnPlay: UIButton!
var gameTitle: UILabel!
var ship = SKSpriteNode()

class TitleScene: SKScene {
    
    override func didMove(to view: SKView) {
        self.backgroundColor = offBlackColor
        setUpText()
        spawnShip()
    }
    
    func spawnShip() {
        ship = SKSpriteNode(imageNamed: "shipPlayer")
        ship.size = CGSize(width: 300, height: 300)
        ship.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        self.addChild(ship)
    }
    
    func setUpText() {
        btnPlay = UIButton(frame: CGRect(x: -self.frame.midX - 50, y: -self.frame.midY + 400, width: 400, height: 100))
//        btnPlay.center = CGPoint(x: (view?.frame.size.width)!, y: 600)
        btnPlay.titleLabel?.font = UIFont(name: "Futura", size: 60)
        btnPlay.setTitle("Play", for: .normal)
        btnPlay.setTitleColor(orangeCustomColor, for: .normal)
        btnPlay.setTitleShadowColor(offBlackColor, for: .normal)
        
        btnPlay.addTarget(self, action: #selector(playTheGame), for: .touchUpInside)
        self.view?.addSubview(btnPlay)
        
        gameTitle = UILabel(frame: CGRect(x: 0, y: 0, width: (view?.frame.width)!, height: 300))
        gameTitle.textColor = offWhiteColor
        gameTitle.font = UIFont(name: "Futura", size: 30)
        gameTitle.textAlignment = .center
        gameTitle.text = "Frog Captain Dat Boi"
        
        self.view?.addSubview(gameTitle)
    }
    
    func playTheGame() {
        self.view?.presentScene(GameScene(), transition: SKTransition.doorway(withDuration: 0.4))
        btnPlay.removeFromSuperview()
        gameTitle.removeFromSuperview()
        
        if let scene = GameScene(fileNamed: "GameScene") {
            let skView = self.view!
            skView.ignoresSiblingOrder = true
            scene.scaleMode = .aspectFill
            skView.presentScene(scene)
        }
    }
    
    
    
}
